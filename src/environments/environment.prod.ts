export const environment = {
  production: true,
  apiUrl: 'https://pets-api.bonfico.fr',
  apiSuffix: '/api/'
}
