import { RelationshipsInterface, DefinitionInterface, DefaultInterface, PaginateInterface } from './';

export interface ModelPetInterface {
    id?: number;
    name: string;
    age: number;
    bio: string;
    created_at: Date;
    updated_at: Date;
}

// /pets/:id
export interface PetInterface extends DefaultInterface {
    data: {
        data: OnePet
        included?: IncludedPetsInterface
    }
}

// /pets
export interface PetsInterface extends DefaultInterface {
    data: DatasPetsInterface;
    included?: IncludedPetsInterface
}

interface OnePet extends RelationshipsInterface, DefinitionInterface {
    attributes: ModelPetInterface;
}

interface DatasPetsInterface extends PaginateInterface {
    data: OnePet[];
}

interface IncludedPetsInterface extends DefinitionInterface {
    attributes: any;
}