import { RelationshipsInterface, DefinitionInterface, DefaultInterface, PaginateInterface } from './';

export interface ModelTypeInterface {
    id?: number;
    name: string;
    age: number;
    bio: string;
    created_at: Date;
    updated_at: Date;
}

// /types/:id
export interface TypeInterface extends DefaultInterface {
    data: {
        data: OneType
        included?: IncludedTypesInterface
    }
}

// /types
export interface TypesInterface extends DefaultInterface {
    data: DatasTypesInterface;
    included?: IncludedTypesInterface
}

interface OneType extends RelationshipsInterface, DefinitionInterface {
    attributes: ModelTypeInterface;
}

interface DatasTypesInterface extends PaginateInterface {
    data: OneType[];
}

interface IncludedTypesInterface extends DefinitionInterface {
    attributes: any;
}