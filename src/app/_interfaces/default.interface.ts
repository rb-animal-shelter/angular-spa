export interface DefaultInterface {
    success: boolean;
    message: string;
}

export interface PaginateInterface {
    meta: {
        pagination: {
            total: number,
            count: number,
            per_page: number,
            current_page: number,
            total_pages: number
        }
    },
    links: {
        self: string,
        fisrt: string,
        last: string
    }
}

export interface DefinitionInterface {
    type: string,
    id: string
}

export interface RelationshipsInterface {
    relationships: {
        [key: string]: { 
            data: DefinitionInterface | DefinitionInterface[];
        }
    }
}

export interface DeletedInterface extends DefaultInterface {
    data: string;
}