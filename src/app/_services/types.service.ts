import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { NetworkService } from './network.service';
import { TypesInterface, TypeInterface } from '../_interfaces';
import { DeletedInterface } from '../_interfaces';

@Injectable()
export class TypesService {
    private target = 'types';
    private url = `${this.networkService.apiUrl}${this.target}`;

    constructor(
        private http: HttpClient,
        private networkService: NetworkService
    ) { }

    public browse(terms?: any): Observable<TypesInterface> {
        return this.http.get<TypesInterface>(`${this.url}`, { params: (terms !== undefined ? terms : {}) });
    }

    public read(id: number, terms?: any): Observable<TypeInterface> {
        return this.http.get<TypeInterface>(`${this.url}/${id}`, { params: (terms !== undefined ? terms : {}) });
    }

    public create(group: any): Observable<TypeInterface> {
        return this.http.post<TypeInterface>(this.url, group);
    }

    public update(group: any): Observable<TypeInterface> {
        return this.http.put<TypeInterface>(`${this.url}/${group.id}`, group);
    }

    public delete(id: number): Observable<DeletedInterface> {
        return this.http.delete<DeletedInterface>(`${this.networkService.apiUrl}${this.target}/${id}`);
    }
}