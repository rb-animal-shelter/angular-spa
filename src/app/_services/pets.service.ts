import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { NetworkService } from './network.service';
import { PetsInterface, PetInterface } from '../_interfaces';
import { DeletedInterface } from '../_interfaces';

@Injectable()
export class PetsService {
    private target = 'pets';
    private url = `${this.networkService.apiUrl}${this.target}`;

    constructor(
        private http: HttpClient,
        private networkService: NetworkService
    ) { }

    public browse(terms?: any): Observable<PetsInterface> {
        return this.http.get<PetsInterface>(`${this.url}`, { params: (terms !== undefined ? terms : {}) });
    }

    public read(id: number, terms?: any): Observable<PetInterface> {
        return this.http.get<PetInterface>(`${this.url}/${id}`, { params: (terms !== undefined ? terms : {}) });
    }

    public create(group: any): Observable<PetInterface> {
        return this.http.post<PetInterface>(this.url, group);
    }

    public update(group: any): Observable<PetInterface> {
        return this.http.put<PetInterface>(`${this.url}/${group.id}`, group);
    }

    public delete(id: number): Observable<DeletedInterface> {
        return this.http.delete<DeletedInterface>(`${this.networkService.apiUrl}${this.target}/${id}`);
    }
}