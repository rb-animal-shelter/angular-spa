import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class NetworkService {
    public apiUrl: string;

    constructor() {
        this.apiUrl = `${environment.apiUrl}${environment.apiSuffix}`;
    }
}