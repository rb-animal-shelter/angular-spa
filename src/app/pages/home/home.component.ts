import { Component, OnInit } from '@angular/core';
import { PetsService } from 'src/app/_services/pets.service';
import { TypesService } from 'src/app/_services/types.service';
import { MatSelectChange } from '@angular/material/select';

@Component({
    selector: 'home-component',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    pets: any;
    types: any;
    filteredDatas = [];
    selectedTypes = [];
    search: string;

    constructor(
        private petsService: PetsService,
        private typesService: TypesService,
    ) { }

    ngOnInit() {
        this.petsService.browse().subscribe((result) => {
            this.pets = result.data.data;
            this.filteredDatas = this.pets;
        });

        this.typesService.browse().subscribe((result) => {
            this.types = result.data.data;
        });
    }

    onTypesChange(event: MatSelectChange) {
        this.selectedTypes = event.value.map(Number);
        this.filterPets();
    }

    onKeypressEvent(event: any){
        this.search = event.target.value;
        this.filterPets();
    }

    getTypeName(type_id: number) {
        if (this.types) return this.types.find(type => type.id == type_id).attributes.name;
    }

    getPetImage(petAttrs) {
        const folder = this.getTypeName(petAttrs.type_id);
        const file = petAttrs.name.toLowerCase();
        if (folder) return './assets/img/'+folder.toLowerCase()+'/'+file+'.jpg';
    }

    filterPets() {
        if (this.search) {
            this.filteredDatas = this.pets.filter(pet => {                
                const petName = pet.attributes.name.toLowerCase();
                const petAge = pet.attributes.age.toString();

                if ((petName.indexOf(this.search) !== -1) || (petAge.indexOf(this.search) !== -1)) {
                    if (this.selectedTypes.length > 0) {
                        if (this.selectedTypes.includes(pet.attributes.type_id)) {
                            return true;
                        }
                    } else {
                        return true;
                    }
                }
            });
        } else {
            if (this.selectedTypes.length > 0) {
                this.filteredDatas = this.pets.filter(pet => this.selectedTypes.includes(pet.attributes.type_id));
            } else {
                this.filteredDatas = this.pets;
            }
        }
    }
}