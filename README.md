# AnimalShelter

This project was built with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.0.

## Requirements
- Node.js (Angular requires a current, active LTS, or maintenance LTS version of Node.js)
- Npm package manager
- Angular CLI
   - Run `ng --version` and make sure Angular CLI is installed
   - If not, run `npm install -g @angular/cli` to install it

## Installation

### Running Angular over HTTPS with a Trusted Certificate

After cloning the repo, navigate through the folder `ssl` and installing the certificate according to the following steps :

**OS X**
1)  Double click on the certificate (server.crt)
2) Select your desired keychain (login should suffice)
3) Add the certificate
4) Open Keychain Access if it isn’t already open
5) Select the keychain you chose earlier
6) You should see the certificate localhost
7) Double click on the certificate
8) Expand Trust
9) Select the option Always Trust in When using this certificate
10) Close the certificate window

**Windows 10**
1) Double click on the certificate (server.crt)
2) Click on the button “Install Certificate …”
3) Select whether you want to store it on user level or on machine level
4) Click “Next”
5) Select “Place all certificates in the following store”
6) Click “Browse”
7) Select “Trusted Root Certification Authorities”
8) Click “Ok”
9) Click “Next”
10) Click “Finish”
11) If you get a prompt, click “Yes”

Before running the application, make sure you have restarted your browser to avoid a certificate problem.

### Laravel API - Base url
All API's requests are made by default from this address : `https://pets-api.dev`.
To change the default API's address, go into `src\environments\environment.ts` and change the `apiUrl` value.

### Build

**Development**
1) Run `npm start` or `ng serve --ssl true` command.
2) Navigate to `https://localhost:4200/`.

**Production**
1) You can also change the API URL base production with going into `src\environments\environment.prod.ts` and change the `apiUrl` value.
2) Run `ng build --prod` command.
2) Copy everything within the output folder (dist/ by default) to a folder on the server.
3) Configure the server to redirect requests for missing files to index.html.

_Note : Make sure the Laravel API is running before using the SPA._